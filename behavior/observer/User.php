<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/2
 * Time: 10:44
 */

namespace behavior\observer;


class User implements \SplSubject
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var \SplObjectStorage
     */
    private $observers;

    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    public function attach(\SplObserver $observer):void
    {
        $this->observers->attach($observer);
    }

    public function detach(\SplObserver $observer):void
    {
        $this->observers->detach($observer);
    }

    public function changeEmail(string $email):void
    {
        $this->email = $email;
        $this->notify();
    }

    public function notify():void
    {
        /** @var \SplObserver $observer */
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}