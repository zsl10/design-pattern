<?php
/**
 * Created by PhpStorm
 * User: zsl <zhangshiliang@mafengwo.com>
 * Date: 2019/1/2
 * Time: 10:45
 */

namespace behavior\observer;


class UserObserver implements \SplObserver
{
    /**
     * @var User[]
     */
    private $changedUsers = [];

    /**
     *
     * @param \SplSubject $subject
     */
    public function update(\SplSubject $subject): void
    {
        $this->changedUsers[] = clone $subject;
    }


    /**
     * @return User[]
     */
    public function getChangedUsers():array
    {
        return $this->changedUsers;
    }
}