<?php
/**
 * Created by PhpStorm
 * Date: 2018/12/29
 * Time: 19:10
 */

class Autoload
{
    public static function load($class)
    {
        require ROOT_DIR.'/'.str_replace('\\', '/', $class).'.php';
    }
}