<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:52
 */

namespace create\factory_method;

interface LoggerFactory
{
    public function createLogger():ILogger;
}