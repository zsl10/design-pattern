<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:51
 */

namespace create\factory_method;


class FileLoggerFactory implements LoggerFactory
{
    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }
    public function createLogger(): ILogger
    {
        return new FileLogger($this->filePath);
    }
}