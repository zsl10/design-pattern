<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:53
 */

namespace create\factory_method;


class StdoutLoggerFactory implements LoggerFactory
{
    public function createLogger(): ILogger
    {
        return new StdoutLogger();
    }
}