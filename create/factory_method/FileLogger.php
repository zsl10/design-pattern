<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:48
 */

namespace create\factory_method;


class FileLogger implements ILogger
{
    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function log(string $message):void
    {
        file_put_contents($this->filePath, $message . PHP_EOL, FILE_APPEND);
    }
}