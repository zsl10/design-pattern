<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:50
 */

namespace create\factory_method;


class StdoutLogger implements ILogger
{
    public function log(string $message):void
    {
        echo $message;
    }
}