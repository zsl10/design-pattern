<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 21:47
 */

namespace create\factory_method;


interface ILogger
{
    public function log(string $message);
}