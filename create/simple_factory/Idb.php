<?php
/**
 * Created by PhpStorm
 * Date: 2019/1/1
 * Time: 16:10
 */

namespace  create\simple_factory;

interface Idb
{
    public function setHost($host);
    public function setDB($db);
    public function setUserName($user);
    public function setPassword($pwd);
    public function connect();
}