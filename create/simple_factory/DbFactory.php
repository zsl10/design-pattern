<?php
/**
 * Created by PhpStorm
 * User: zsl <zhangshiliang@mafengwo.com>
 * Date: 2019/1/1
 * Time: 16:18
 */

namespace create\simple_factory;


class DbFactory
{
    protected $driver = null;

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function makeDB($host, $user, $pass, $dbname)
    {
        $DB=null;
        if ($this->driver === 'mysql') {
            $DB = new MysqlDb();
        } elseif ($this->driver === 'postgre') {
            $DB = new PostGreSqlDb();
        } elseif ($this->driver === 'sqlite') {
            $DB = new SqLite();
        }

        $DB->setHost($host);
        $DB->setDB($dbname);
        $DB->setUserName($user);
        $DB->setPassword($pass);
        $DB->connect();

        return $DB;
    }
}