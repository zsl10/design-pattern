<?php
/**
 * Created by PhpStorm
 * User: zsl <zhangshiliang@mafengwo.com>
 * Date: 2019/1/1
 * Time: 16:14
 */

namespace create\simple_factory;


class MysqlDb implements Idb
{
    public function setHost($host)
    {
        // code
    }

    public function setDB($db)
    {
        // code
    }

    public function setUserName($user)
    {
        // code
    }

    public function setPassword($pwd)
    {
        // code
    }

    public function connect()
    {
        // code
    }
}